#include <vector>
#include "Drawing.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace std;

int InitApp(std::vector<float>, std::vector<float>);

class Matrix
{
public:
	float** createRotationXMatrix(float corner) {
		float** matrix;

		matrix = new float* [4];

		for (int i = 0; i < 4; i++) {
			matrix[i] = new float[4];
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}

		matrix[0][0] = 1.0f;
		matrix[1][1] = cos(corner);
		matrix[1][2] = -sin(corner);
		matrix[2][1] = sin(corner);
		matrix[2][2] = cos(corner);
		matrix[3][3] = 1.0f;

		return matrix;
	}
	float** createRotationYMatrix(float corner) {
		float** matrix;

		matrix = new float* [4];

		for (int i = 0; i < 4; i++) {
			matrix[i] = new float[4];
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}

		matrix[0][0] = cos(corner);
		matrix[0][2] = sin(corner);
		matrix[2][0] = -sin(corner);
		matrix[2][2] = cos(corner);
		matrix[1][1] = 1.0f;
		matrix[3][3] = 1.0f;

		return matrix;
	}
	float** createRotationZMatrix(float corner) {
		float** matrix;

		matrix = new float* [4];

		for (int i = 0; i < 4; i++) {
			matrix[i] = new float[4];
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}

		matrix[0][0] = cos(corner);
		matrix[0][1] = -sin(corner);
		matrix[1][0] = sin(corner);
		matrix[1][1] = cos(corner);
		matrix[2][2] = 1.0f;
		matrix[3][3] = 1.0f;

		return matrix;
	}

	float** createMainRotationMatrix(float rx, float ry, float rz, float corner) {
		float** matrix;

		matrix = new float* [4];

		for (int i = 0; i < 4; i++) {
			matrix[i] = new float[4];
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}

		matrix[0][0] = cos(corner) + pow(rx,2) * (1 - cos(corner));
		matrix[0][1] = rx * ry * (1 - cos(corner)) - rz * sin(corner);
		matrix[0][2] = rx * rz * (1 - cos(corner)) + ry * sin(corner);
		matrix[1][0] = ry * rx * (1 - cos(corner)) + rz * sin(corner);
		matrix[1][1] = cos(corner) + pow(ry, 2) * (1 - cos(corner));
		matrix[1][2] = ry * rz * (1 - cos(corner)) - rx * sin(corner);
		matrix[2][0] = rx * rz * (1 - cos(corner)) - ry * sin(corner);
		matrix[2][1] = ry * rz * (1 - cos(corner)) + rx * sin(corner);
		matrix[2][2] = cos(corner) + pow(rz, 2) * (1 - cos(corner));
		matrix[3][3] = 1.0f;
		return matrix;
	}

	float* createVector(float x, float y, float z, float a)
	{
		float* vector;

		vector = new float [4];

		vector[0] = x;
		vector[1] = y;
		vector[2] = z;
		vector[3] = 1.0f;

		return vector;
	}

	float** createMatrix(float x, float y, float z)
	{
		float** matrix;

		matrix = new float* [4];

		for (int i = 0; i < 4; i++) {
			matrix[i] = new float[4];
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}

		matrix[0][0] = x;
		matrix[1][1] = y;
		matrix[2][2] = z;
		matrix[3][3] = 1.0f;

		return matrix;
	}

	float** myltiplyMatixOnScalar(float** matrix, float scalar)
	{
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] *= scalar;
			}
		}

		return matrix;
	}

	float* myltiplyMatrixOnVector(float** matrix, float* vector)
	{
		float* resVector;

		resVector = new float[4];

		for (int i = 0; i < 4; i++) {
			resVector[i] = 0.0f;
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				resVector[i] += matrix[i][j] * vector[i];
			}
		}

		return resVector;
	}
};

int main() {
	DrawingData dd = DrawingData{};

	Matrix m;

	const float x = 0.5f, y = 0.6f, z = 0.7f, scalar = 0.5f, corner = 30.0f, Rx = 0.5f, Ry = 0.6f, Rz = 0.7f;

	float* a = m.createVector(x, y, z, 1.0f);
	cout << "Create vector" << endl;
	for (int i = 0; i < 4; i++) {
		cout << a[i] << "\t";
	}
	cout << "\n\n\n" << endl;

	float** b = m.createMatrix(x, y, z);
	cout << "Create matrix: " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << b[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	float** c = m.myltiplyMatixOnScalar(b, scalar);
	cout << "Myltiply matrix on scalar " << scalar << ":" << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << c[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	float* d = m.myltiplyMatrixOnVector(b, a);
	cout << "Myltiply matrix on vector:" << endl;
	for (int i = 0; i < 4; i++) {
		cout << d[i] << "\t";
	}
	cout << "\n\n\n" << endl;

	float** rx = m.createRotationXMatrix(corner);
	cout << "Create rotation matrix (x): " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << rx[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	float** ry = m.createRotationYMatrix(corner);
	cout << "Create rotation matrix (y): " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << ry[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	float** rz = m.createRotationZMatrix(corner);
	cout << "Create rotation matrix (z): " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << rz[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	float** r = m.createMainRotationMatrix(Rx, Ry, Rx, corner);
	cout << "Create big rotation matrix: " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << r[i][j] << "\t";
		}
		cout << endl;
	}
	cout << "\n\n" << endl;

	
	
	
	vector<float> vertices = dd.GetDrawingTrianglesData();
	vector<float> vertices_lines = dd.GetDrawingLinesData();
	InitApp(vertices, vertices_lines);
	return 0;
}