#include <vector>
#include "Drawing.h"
#include <iostream>

int InitApp(std::vector<float>, std::vector<float>);

void board(DrawingData& dd) {
    double x1 = -1.0f, y1 = 1.0f, x2 = -0.75f, y2 = 1.0f, x3 = -0.75f, y3 = 0.75f, x4 = -1.0f, y4 = 0.75f, c = 0.0f;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            dd.AddTriangle(x1, y1, x2, y2, x4, y4, c, c, c);
            dd.AddTriangle(x2, y2, x3, y3, x4, y4, c, c, c);

            x1 += 0.25f, x2 += 0.25f, x3 += 0.25f, x4 += 0.25f;

            if (j % 2 == 0 && i % 2 == 0) c = 1.0f;
            else if (j % 2 != 0 && i % 2 != 0) c = 1.0f;
            else c = 0.0f;
        }

        y1 -= 0.25f, y2 -= 0.25f, y3 -= 0.25f, y4 -= 0.25f;
        x1 = -1.0f, x2 = -0.75f, x3 = -0.75f, x4 = -1.0f;

        if (c == 0.0f) c = 1.0f;
        else c = 0.0f;
    }
}

void circle(const float pi, const float r, DrawingData& dd) {
    for (float t = -pi; t <= pi; t += 0.01) {
        float x1 = cos(t) * r;
        float y1 = sin(t) * r;
        float x2 = cos(t + 0.01f) * r;
        float y2 = sin(t + 0.01f) * r;
        dd.AddLine(x1, y1, x2, y2, 0.0f, 1.0f, 0.0f);
    }
}

void filledCircle(const float pi, const float r, DrawingData& dd) {
    for (float t = -pi; t <= pi; t += 0.01) {
        float x1 = cos(t) * r;
        float y1 = sin(t) * r;
        float x2 = cos(t + 0.01f) * r;
        float y2 = sin(t + 0.01f) * r;
        dd.AddTriangle(x1, y1, x2, y2, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    }
}

void geksaGran(const float pi, const float r, DrawingData& dd) {
    for (float i = 0.0f; i < 1.0f; i += 0.6f) {
        for (float t = -pi; t <= pi; t += pi / 3) {
            float x1 = cos(t) * r;
            float y1 = sin(t) * r;
            float x2 = cos(t + pi / 3) * r;
            float y2 = sin(t + pi / 3) * r;
            dd.AddLine(x1 + i, y1, x2 + i, y2, 0.0f, 1.0f, 0.0f);
        }
    }
    for (float j = -0.17f; j < 0.34f; j += 0.34f) {
        for (float t = -pi; t <= pi; t += pi / 3) {
            float x1 = cos(t) * r;
            float y1 = sin(t) * r;
            float x2 = cos(t + pi / 3) * r;
            float y2 = sin(t + pi / 3) * r;
            dd.AddLine(x1 + 0.3, y1 + j, x2 + 0.3, y2 + j, 0.0f, 1.0f, 0.0f);
        }
    }
}

int main() {
	DrawingData dd = DrawingData{};

    const float r = 0.2;
    const float pi = 3.14159265358979323846f;

    /*board(dd);*/
    /*circle(pi, r, dd);*/
    /*filledCircle(pi, r, dd);*/
    /*geksaGran(pi, r, dd);*/

	std::vector<float> vertices = dd.GetDrawingTrianglesData();
	std::vector<float> vertices_lines = dd.GetDrawingLinesData();
	std::cout << sizeof(vertices);
	InitApp(vertices, vertices_lines);
	return 0;
}